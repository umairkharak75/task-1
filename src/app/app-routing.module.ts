import { AppComponent } from './app.component';


import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MatSidenavModule} from '@angular/material/sidenav';

const routes: Routes = [
  {path: '', redirectTo: 'app', pathMatch: 'full'},
  {path: 'app', loadChildren: () => import('./modules/main-dashboard/main-dashboard.module').then(m => m.MainDashboardModule)},

     
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    MatSidenavModule

],
  exports: [RouterModule],
})
export class AppRoutingModule {}
