import { MainDashboardComponent } from './main-dashboard.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [


  { path: '', component: MainDashboardComponent, 
children: [
  { path: '', redirectTo: 'dashboard' },
  { path: 'dashboard',component:DashboardComponent }
 
]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainDashboardRoutingModule { }
