import { SharedModule } from './../shared/shared.module';
import { MainDashboardComponent } from './main-dashboard.component';
import { RouterModule } from '@angular/router';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainDashboardRoutingModule } from './main-dashboard-routing.module';
import { CoreModule } from '../core/core.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';


@NgModule({
  declarations: [MainDashboardComponent, DashboardComponent],
  imports: [
    CoreModule,
    CommonModule,
    MainDashboardRoutingModule,
    MatBadgeModule,
    MatButtonModule,
    IvyCarouselModule,
    RouterModule,
    SharedModule

  ]
})
export class MainDashboardModule { }
