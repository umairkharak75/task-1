import { CoreComponent } from './core.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { LeftSideBarComponent } from './components/left-side-bar/left-side-bar.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [CoreComponent, HeaderComponent, LeftSideBarComponent],
  imports: [
    CommonModule,
    CoreRoutingModule,
    MatSidenavModule,
    MatInputModule,
    MatButtonModule

  ],
  exports:[HeaderComponent,LeftSideBarComponent]
})
export class CoreModule { }
